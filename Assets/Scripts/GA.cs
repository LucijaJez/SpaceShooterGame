﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;

public class GA
{ 
    public int currentGenome;  //0 do maximalnog broja populacije
    private int genomeID;  //0 pa na dalje

    public int totalPopulation;     //broj genoma u populaciji
    public int generation;  //trenutna generacija
    private int totalGenomeWeights;    //ukupan broj tezina jednog genoma
    public float mutationRate;     //stopa mutacije
    private float modification;     //maksimalna promjena kod mutacije


    public List<Genome> population = new List<Genome>();  //stvara listu genoma tj populaciju
    public List<int> crossoverSplits = new List<int>();     //dijelovi genoma


    public GA(int totalWeights)  //konstruktor, postavlja na defaultne vrijednosti
    {
        currentGenome = -1;
        totalPopulation = 0;
        totalGenomeWeights = totalWeights;
        genomeID = 0;
        generation = 1;
        mutationRate = 0.3f;
        modification = 0.2f;
    }



    public void GenerateNewPopulation(int totalPopulation)   //poziva se samo na pocetku,stvara prvu populaciju
    {
        generation = 1;
        population.Clear();
        currentGenome = -1;
        this.totalPopulation = totalPopulation;

        for (int i = 0; i < totalPopulation; i++)   //stvori svaki od genoma,postavi mu fitness na 0, ID i napuni ga random weightsima
        {
            Genome genome = new Genome();
            genome.ID = genomeID;
            genomeID++;
            genome.fitness = 0.0f;
            genome.weights = new List<float>();
            for (int j = 0; j < totalGenomeWeights; j++)
            {
                genome.weights.Add(Random.Range(-1f, 1f));
            }
            population.Add(genome);     //doda genom u populaciju
        }
    }



    public Genome GetNextGenome()  //vraca sljedeci genom ako je indeks manji od velicine populacije
    {
        currentGenome++;
        if (currentGenome >= population.Count)
            return null;
        return population[currentGenome];
    }



    public Genome GetBestGenome()  //pronalazi najbolji genom u populaciji i vraca ga
    {
        int bestGenome = 0;  //postavlja prvi genom za najbolji
        float fitness=0;
        for (int i = 0; i < population.Count; i++)  //prolazi kroz cijelu populaciju
        {
            if (population[i].fitness >= fitness)  //ako ima bolji fitness od trenutno spremljenoga, postavi novi fitness i spremi njegov index
            {
                fitness = population[i].fitness;
                bestGenome = i;
            }
        }
        return population[bestGenome];  //vrati genom s tim indeksom
    }


 
    public void CrossBreed(Genome g1, Genome g2, ref Genome baby1, ref Genome baby2)    //dobiva djecu od 2 roditelja
    {
        int crossover = (int)Random.Range(1, totalGenomeWeights - 2);  //odredi mjesto granice weightsa

        baby1 = new Genome();   //napravi prvo novo dijete i postavi mu novi ID
        baby1.ID = genomeID;
        baby1.weights = new List<float>();      
        genomeID++;

        baby2 = new Genome();   //drugo dijete
        baby2.ID = genomeID;
        baby2.weights = new List<float>();
        genomeID++;

        for (int i = 0; i < crossover; i++)     //prvi dio, od pocetka weightsa do crossovera
        {
            baby1.weights.Add(g1.weights[i]);      //prvo dijete dobiva prvi dio od prvog roditelja
            baby2.weights.Add(g2.weights[i]);       //drugo dijete dobiva prvi dio od drugog roditelja
        }

        for (int i = crossover; i < totalGenomeWeights; i++)    //drugi dio, od granice do zadnjeg weighta
        {
            baby1.weights.Add(g2.weights[i]);    //prvo dijete dobiva drugi dio od drugog roditelja
            baby2.weights.Add(g1.weights[i]);    //drugo dijete dobiva drugi dio od prvog roditelja
        }
    }



    public Genome CreateNewGenome(int totalWeights)   //stvara novi genom, postavi mu fitness na 0, ID i napuni ga random weightsima i vraca ga
    {
        Genome genome = new Genome();
        genome.ID = genomeID;
        genome.fitness = 0.0f;
        genome.weights = new List<float>();
        for (int i = 0; i < totalWeights; i++)
        {
            genome.weights.Add(Random.Range(-1f, 1f));
        }
        genomeID++;
        return genome;
    }



    public void BreedPopulation()   //glavna funkcija koja sprema iducu generaciju
    {
        List<Genome> bestGenomes = population.OrderByDescending(o=>o.fitness).ToList();  //sortirana lista po najboljim genomima
        List<Genome> children = new List<Genome>();

        WriteBestIntoFile(bestGenomes[0]);

        bestGenomes[0].fitness = 0.0f;   //najbolji genom, ide u sljedecu populaciju, fitness mu postavljam na 0
        children.Add(bestGenomes[0]);   //doda prvog najboljega u children

        bestGenomes[1].fitness = 0.0f;   //drugi najbolji genom, ide  u sljedecu populaciju, fitness mu postavljam na 0
        children.Add(bestGenomes[1]);   //doda drugog najboljega u children

        Genome baby1 = new Genome();    //dva genoma za dvoje djece od svakog para
        Genome baby2 = new Genome();

        CrossBreed(bestGenomes[0], bestGenomes[1], ref baby1, ref baby2); //prvi par, dobije 2 djece od 2 roditelja
        Mutate(baby1);  //mutira gene od svakog djeteta
        Mutate(baby2);
        children.Add(baby1);
        children.Add(baby2);

        CrossBreed(bestGenomes[0], bestGenomes[2], ref baby1, ref baby2); //drugi par roditelja
        Mutate(baby1);
        Mutate(baby2);
        children.Add(baby1);
        children.Add(baby2);

        CrossBreed(bestGenomes[1], bestGenomes[2], ref baby1, ref baby2); //treci par roditelja
        Mutate(baby1);
        Mutate(baby2);
        children.Add(baby1);
        children.Add(baby2);

        if (generation > 10)    //prvih 10 generacije kriza 3 najbolja roditelja, a ostali su slučajni, a nakon 10te kriza 4 roditelja
        {
            CrossBreed(bestGenomes[0], bestGenomes[3], ref baby1, ref baby2); //cetvrti par roditelja
            Mutate(baby1);
            Mutate(baby2);
            children.Add(baby1);
            children.Add(baby2);

            CrossBreed(bestGenomes[1], bestGenomes[3], ref baby1, ref baby2); //peti par roditelja
            Mutate(baby1);
            Mutate(baby2);
            children.Add(baby1);
            children.Add(baby2);

            CrossBreed(bestGenomes[2], bestGenomes[3], ref baby1, ref baby2); //sesti par roditelja
            Mutate(baby1);
            Mutate(baby2);
            children.Add(baby1);
            children.Add(baby2);
        }

        int remainingPopulation = (totalPopulation - children.Count);   //ostatak populacije popuni s random genomima
        for (int i = 0; i < remainingPopulation; i++)
        {
            children.Add(CreateNewGenome(totalGenomeWeights));
        }

        population.Clear();   //izbrise prethodnu populaciju
        population = children;  //postavi djecu za novu populaciju

        currentGenome = -1;  //postavi se na -1, dok se pozove nextGenome da pokrene nultoga
        generation++;
    }



    public void Mutate(Genome genome)    //mijenja vrijednosti nekih tezina
    {
        for (int i = 0; i < genome.weights.Count; i++)  //prolazi kroz sve tezine
        {
            if (Random.Range(0f, 1f) < mutationRate)     //generira se slucajan broj i ako je manji od stope mutacije, ta tezina se zamjeni novom
            {
                genome.weights[i] += Random.Range(-modification, modification);
                if (genome.weights[i] > 1)
                    genome.weights[i] = 1;
                if (genome.weights[i] < -1)
                    genome.weights[i] = -1;
            }
        }
    }



    public void SetGenomeFitness(float fitness, int index)   //postavlja fitness genomu
    {
        if (index >= population.Count)
            return;
        else
            population[index].fitness = fitness;
    }



    public Genome GetGenome(int index)   //vraca genom za zadani indeks
    {
        if (index >= totalPopulation)
            return null;
        return population[index];
    }



    public int GetCurrentGenomeIndex()  //vraca indeks trenutnog genoma, 0 do populationSize
    {
        return currentGenome;
    }



    public int GetCurrentGenomeID()  //vraca ID trenuntog genoma, genomeID
    {
        return population[currentGenome].ID;
    }



    public int GetCurrentGeneration()  //vraca trenutnu generaciju
    {
        return generation;
    }



    public int GetTotalPopulation()   //vraca ukupan broj genoma u populaciji
    {
        return totalPopulation;
    }



    public void WriteBestIntoFile(Genome genome)   //zapisuje tezine i fitness najboljeg genoma iz svake generacije u datoteku
    {
        string path = "Najbolji.txt";
        using (StreamWriter writer = File.AppendText(path))  //pisati ce na kraj datoteke
        {
            writer.Write("\nf" + genome.fitness);     //zapisuje fitness genoma pocevsi red sa f
            for (int i=0;i<totalGenomeWeights;i++)    //u svakom redu koji zapocinje sa w zapisuje se jedna tezina genoma
                 writer.Write("\nw"+genome.weights[i]);
            writer.Close();
        }
    }
}