﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using System.Linq;
using System.IO;


public class NNet
{
    List<float> inputs = new List<float>();
    List<float> outputs = new List<float>();
    NLayer hiddenLayer = new NLayer();
    NLayer outputLayer = new NLayer();

    public void Calculate() //poziva evaluate za hidden i za output layer, dobiva izlaze za ulaze
    {
        outputs.Clear();
        hiddenLayer.Evaluate(inputs, ref outputs);

        inputs = outputs;   //izlazi iz hiddena su ulazi za output layer
        outputLayer.Evaluate(inputs, ref outputs);  //dobiva konacne izlaze
    }

    public void SetInput(List<float> input)     //postavlja ulaze u mrezu
    {
        inputs = input;
    }

    public float GetOutput(int index)      //vraca izlaz iz liste izlaza za zadani index
    {
        return outputs[index];
    }

    public void FromGenome(Genome genome, int numofInputs, int neuronsPerHidden, int numOfOutputs)  //razvrstava genom u neurone
    {
        List<Neuron> neurons = new List<Neuron>();
        
        for (int i = 0; i < neuronsPerHidden; i++)  //puni neurone hidden layera, ide po neuronima
        {
            neurons.Add(new Neuron());  //stvara neuron
            List<float> weights = new List<float>();    //stvara pomocnu listu za tezine

            for (int j = 0; j <= numofInputs; j++) //ide po tezinama od neurona,ulazi u taj neuron + bias
            {
                weights.Add(0.0f);
                weights[j] = genome.weights[i * (numofInputs+1) + j];
            }
            neurons[i].weights = new List<float>();     //stvara listu weightsa za neuron
            neurons[i].Initialise(weights, numofInputs);    //postavlja vrijednosti u neuron
        }
        hiddenLayer.LoadLayer(neurons);     //stavlja te neurone u hidden layer

        List<Neuron> outneurons = new List<Neuron>();

        for (int i = 0; i < numOfOutputs; i++) //isto za neurone output layera
        {
            outneurons.Add(new Neuron());
            List<float> weights = new List<float>();

            for (int j = 0; j <= neuronsPerHidden; j++)
            {
                weights.Add(0.0f);
                int start = (numofInputs+1)*neuronsPerHidden;  //zato da cita iz genoma preostale weightse
                weights[j] = genome.weights[start + i * (neuronsPerHidden+1) + j];
            }
            outneurons[i].weights = new List<float>();
            outneurons[i].Initialise(weights, neuronsPerHidden);
        }
        outputLayer.LoadLayer(outneurons);
    }
}



public class NLayer
{
    private int layerNeurons;
    private int neuronInputs;
    List<Neuron> neurons = new List<Neuron>();

    public void SetNeurons(List<Neuron> neurons, int numOfNeurons, int numOfInputs)
    {
        neuronInputs = numOfInputs;
        layerNeurons = numOfNeurons;
        this.neurons = neurons;
    }

    public float BiPolarSigmoid(float a)
    {
        return (-1 + 2 / (1 + Mathf.Exp( -a )));
    }
    
    public int getLayerNeurons()
    {
        return layerNeurons;
    }

    public void Evaluate(List<float> input, ref List<float> output)
    {
        for (int i = 0; i < layerNeurons; i++)     //do broja neurona od sloja nad kojim poziva evaluate
        {
            float activation = 0.0f;    //izlaz za jedan neuron

            for (int j = 0; j < neurons[i].numInputs; j++)
            {
                activation += input[j] * neurons[i].weights[j];     //mnozi ulaz neurona s pripadajucom tezinom
                //Debug.Log(activation +"="+input[j]+"*"+ neurons[i].weights[j]);
            }
            activation += 1 * neurons[i].weights[neurons[i].numInputs];   //bias
            output.Add(BiPolarSigmoid(activation));   //primjeni prijenosnu funkciju nad activation i stavlja to u listu izlaza
        }
    }

    public void LoadLayer(List<Neuron> input)
    {
        layerNeurons = input.Count;
        neurons = input;
    }
}



public class Neuron     //definiran je tezinama kojih ima koliko i ulaza u  neuron + 1 (bias)
{
    public int numInputs;
    public List<float> weights = new List<float>();

    public void Initialise(List<float> weights, int numInputs)  //tezine i broj ulaza se postavljaju u neuron
    {
        this.numInputs = numInputs;
        this.weights = weights;
    }
}



public class Genome     //sadrži fitness,ID i tezine
{
    public float fitness;
    public int ID;
    public List<float> weights;

    public override string ToString()   //vraca genom kao string
    {
        string helpStr = "\nFitness: " + fitness + " ID: " + ID + "Weights: " + ListToString();
        return helpStr;
    }

    public string ListToString()    //vraca string od liste tezina
    {
        string helpStr = "";
        foreach (var w in weights)
        {
            helpStr += "\n " + w;
        }
        return helpStr;
    }
}

