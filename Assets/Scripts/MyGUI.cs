﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MyGUI : MonoBehaviour
{
    public static int minutes, seconds;
    public static int score,asteroidScore;
    public static float timer;
    public static GUIStyle gui;
    public static float timeHeDied;


    void Start ()
    {
        gui = new GUIStyle();
        gui.fontSize = 30;
        gui.normal.textColor = Color.white;
        timer = 0f;
        score = 0;
        asteroidScore = 0;
        timeHeDied = 0;
	}
	

	void Update ()
    {
        timer = Time.timeSinceLevelLoad - timeHeDied;
        score = minutes*60 + seconds + asteroidScore*3;
    }


    void OnGUI()
    {
        minutes = Mathf.FloorToInt(timer / 60F);
        seconds = Mathf.FloorToInt(timer - minutes * 60);
        string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);

        GUI.Label(new Rect(10, 10, 250, 150),"Time: " + niceTime,gui);
        GUI.Label(new Rect(10, 45, 250, 150),"Asteroids: " + asteroidScore.ToString(),gui);
        GUI.Label(new Rect(10, 80, 250, 150),"Score: " + score.ToString(),gui);
    }
}
