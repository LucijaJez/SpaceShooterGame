﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 10f;
    private Rigidbody2D rb;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        GameObject player = GameObject.Find("SpaceTriangle");
        Transform playerTransform = player.transform;
        rb.transform.rotation = playerTransform.rotation;   //metak je rotiran kako i agent
        rb.velocity = playerTransform.up*speed;     //puca metak iz vrha svemirskog broda brzinom speed
    }


    void OnTriggerEnter2D(Collider2D other)     //u sudaru sa asteroidom poveca se asteroidScore i unistavaju i metak i asteroid
    {
        if (other.gameObject.tag == "Enemy")
        {
            MyGUI.asteroidScore += 1;
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }


    void OnBecameInvisible()    //unisti metak dok ga vise ne vidimo
    {
        Destroy(gameObject);
    }


    void Update ()
    {	
	}
}
