﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SpaceTriangle : MonoBehaviour
{
    private float speed = 6f;
    private float rotateSpeed = 8f;
    public LayerMask whatToHit;
    public float range = 6f;
    public float h;
    public float v;
    private Rigidbody2D activeAgent;
    public GameObject bullet;
    public float fireRate = 0.6f;
    private float nextFire = 0.0f;


    void Start ()
    {
        activeAgent = GetComponent<Rigidbody2D>();
        activeAgent.freezeRotation = true;
    }


    void Awake()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
    }


    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Destroy(gameObject);
            string scene = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene(scene, LoadSceneMode.Single);
        }
    }


    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene("Main menu");
        }

        /* 
        Drugi nacin kretanja
        activeAgent.velocity = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * speed;
        Vector2 vec = activeAgent.velocity;
        if (vec != Vector2.zero)
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Vector3.forward, vec), Time.fixedDeltaTime * rotateSpeed);
       */
        
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            h = 1;
        else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            h = -1;
        else h = 0;
        transform.Rotate(Vector3.forward *h*-rotateSpeed);

        v = Input.GetAxis("Vertical");
        if (v>=0)
             activeAgent.velocity = transform.up * v * speed;
        
        
        if (Input.GetKey(KeyCode.Space) && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(bullet, transform.position, Quaternion.identity);
        }
    }
}
