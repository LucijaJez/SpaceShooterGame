﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    int index = 0;


    void Awake()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
    }


    void Start()
    {
    }


    public void SetDropdownIndex(int index)
    {
        this.index = index;
    }


    public void playGame()
    {
        switch (index)
        {
            case 0:
                SceneManager.LoadScene("Space1");
                break;
            case 1:
                SceneManager.LoadScene("Space2");
                break;
            case 2:
                SceneManager.LoadScene("Space3");
                break;
        }
    }


    public void quitGame()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }
}
