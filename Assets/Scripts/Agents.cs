﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.IO;
using System;

public class Agents : MonoBehaviour
{
    private Rigidbody2D activeAgent;
    public LayerMask whatToHit;
    public LineRenderer line1;
    public LineRenderer line2;
    public LineRenderer line3;
    public LineRenderer line4;
    public LineRenderer line5;
    public LineRenderer line6;
    public LineRenderer line7;
    public LineRenderer line8;
    public GameObject bullet;
    public GameObject[] asteroids;

    public static float activeAgentFitness;
    public static float bestFitness;
    public static float generationBestFitness;
    public static GUIStyle gui;

    private float speed = 6f;
    private float rotateSpeed = 8f;
    public float range = 6f;
    public float fireRate = 0.6f;
    private float nextFire = 0.0f;
    private Vector3 defaultP;
    private Quaternion defaultR;
    private float defaultLineWidth = 0.02f;

    public NNet neuralNet;
    public static GA geneticAlgorithm;
    public Genome genome;
    private int totalWeights;
    private int populationSize = 16;
    private int RandomFileNumber;
    private int evolve = 0;
    public bool hasFailed = false;
    float x, y, z;
    StreamWriter writer;

    RaycastHit2D hitU;
    RaycastHit2D hitD;
    RaycastHit2D hitR;
    RaycastHit2D hitL;
    RaycastHit2D hitUR;
    RaycastHit2D hitUL;
    RaycastHit2D hitDR;
    RaycastHit2D hitDL;
    List<float> inputs;


    public void OnGUI()  //ispis
    {
        GUI.Label(new Rect(10, 125, 250, 100), "GenerationBestFitness: " + generationBestFitness,gui);
        GUI.Label(new Rect(10, 160, 250, 100), "BestFitness: " + bestFitness,gui);
        GUI.Label(new Rect(10, 195, 250, 100), "Genome: " + (geneticAlgorithm.currentGenome + 1) + " of " + geneticAlgorithm.totalPopulation,gui);
        GUI.Label(new Rect(10, 230, 250, 100), "Generation: " + geneticAlgorithm.generation,gui);
    }



    void Awake()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
    }



    void OnCollisionEnter2D(Collision2D other)     //kod kolizije s elementom oznake Enemy postavi hasFailed
    {
        if (other.gameObject.tag == "Enemy")
            hasFailed = true;
    }



    public float Normalise(float i)  //svodi ulaze na vrijednosti izmedju -1 i 1
    {
        if (i == 0)
            return 1;       //ako zraka ne pogađa asteroid, tj ako je vrijednost 0, vrati 1
        else return 2 * i / range - 1;     //inace vraca 2*distance / maksimalna duljina zrake -1
    }
    


    private void setUpLines()   //postavlja linije od rayCastova za iscrtavanje
    {
        line1.startWidth = defaultLineWidth;
        line1.endWidth = defaultLineWidth;
        line1.positionCount = 2;
        line1.startColor = Color.red;
        line1.endColor = Color.red;
        line2.startWidth = defaultLineWidth;
        line2.endWidth = defaultLineWidth;
        line2.positionCount = 2;
        line2.startColor = Color.green;
        line2.endColor = Color.green;
        line3.startWidth = defaultLineWidth;
        line3.endWidth = defaultLineWidth;
        line3.positionCount = 2;
        line3.startColor = Color.blue;
        line3.endColor = Color.blue;
        line4.startWidth = defaultLineWidth;
        line4.endWidth = defaultLineWidth;
        line4.positionCount = 2;
        line4.startColor = Color.magenta;
        line4.endColor = Color.magenta;
        line5.startWidth = defaultLineWidth;
        line5.endWidth = defaultLineWidth;
        line5.positionCount = 2;
        line5.startColor = Color.yellow;
        line5.endColor = Color.yellow;
        line6.startWidth = defaultLineWidth;
        line6.endWidth = defaultLineWidth;
        line6.positionCount = 2;
        line6.startColor = Color.white;
        line6.endColor = Color.white;
        line7.startWidth = defaultLineWidth;
        line7.endWidth = defaultLineWidth;
        line7.positionCount = 2;
        line7.startColor = Color.cyan;
        line7.endColor = Color.cyan;
        line8.startWidth = defaultLineWidth;
        line8.endWidth = defaultLineWidth;
        line8.positionCount = 2;
        line8.startColor = Color.gray;
        line8.endColor = Color.gray;
    }



    void Start()
    {
        gui = new GUIStyle();
        gui.fontSize = 30;
        gui.normal.textColor = Color.white;
        setUpLines();
        hasFailed = false;
        activeAgent = GetComponent<Rigidbody2D>();    //dohvaca element kojem pripada skripta i stavlja ga u activeAgent
        activeAgent.freezeRotation = true;        //da se ne rotira u slucaju kolizija

        totalWeights = 8 * 15 + 15 * 3 + 15 + 3;  //ukupan broj tezina
        geneticAlgorithm = new GA(totalWeights);   //poziva  konstruktor
        geneticAlgorithm.GenerateNewPopulation(populationSize);   //tu se zada broj genoma u populaciji i napravi popuacija
        activeAgentFitness = 0.0f;
        generationBestFitness = 0.0f;
        bestFitness = 0.0f;
        inputs = new List<float>();
        neuralNet = new NNet();     //napravi neuronsku mrezu tj.layere
        genome = geneticAlgorithm.GetNextGenome();   //dohvati prvi genom
        neuralNet.FromGenome(genome, 8,15,3);    //rastavi genom na komponente i postavi te vrijednosti u neurone, tu se zada koliko ima neurona

        defaultP = transform.position;     //spremi pocetnu poziciju i rotaciju kod pokretanja igre
        defaultR = transform.rotation;
        //Debug.Log(geneticAlgorithm.GetCurrentGenomeIndex());
        //Debug.Log(geneticAlgorithm.GetCurrentGenomeID());
    }



    void Update()       //za svaki frame
    {
        
        if (Input.GetKey(KeyCode.Escape))   //sa Esc se vrati na izbornik
        {
            SceneManager.LoadScene("Main menu");
        }

        if (!hasFailed)     //ako je agent ziv
        {
            hitU = Physics2D.Raycast(transform.position, transform.up, range, whatToHit);  //gore
            hitD = Physics2D.Raycast(transform.position, -transform.up, range, whatToHit);    //dolje
            hitR = Physics2D.Raycast(transform.position, transform.right, range, whatToHit);   //desno
            hitL = Physics2D.Raycast(transform.position, -transform.right, range, whatToHit);  //lijevo
            hitUR = Physics2D.Raycast(transform.position, transform.up + transform.right, range, whatToHit);   //gore desno
            hitUL = Physics2D.Raycast(transform.position, transform.up - transform.right, range, whatToHit);  //gore lijevo
            hitDR = Physics2D.Raycast(transform.position, -transform.up + transform.right, range, whatToHit);  //dolje desno
            hitDL = Physics2D.Raycast(transform.position, -transform.up - transform.right, range, whatToHit);   //dolje lijevo
            inputs.Clear();
            inputs.Add(Normalise(hitU.distance)); inputs.Add(Normalise(hitD.distance)); //iz hitova uzima udaljenosti, normalizira ih, i stavlja ih u listu inputs
            inputs.Add(Normalise(hitR.distance)); inputs.Add(Normalise(hitL.distance));
            inputs.Add(Normalise(hitUR.distance)); inputs.Add(Normalise(hitUL.distance));
            inputs.Add(Normalise(hitDR.distance)); inputs.Add(Normalise(hitDL.distance));

            neuralNet.SetInput(inputs);     //postave se ulazi u neuronsku mrežu
            neuralNet.Calculate();     //izracunaju se izlazi

            x = neuralNet.GetOutput(0);     //rotacija
            y = neuralNet.GetOutput(1);     //ide li naprijed ili miruje
            z = neuralNet.GetOutput(2);     //da li puca ili ne

            //Debug.Log("x" + x);
            //Debug.Log("y" + y);
            //Debug.Log("z" + z);

            if (x < -0.33) 
                x = -1;     //rotira lijevo
            else if (x > 0.33)
                x = 1;      //rotira desno
            else x = 0;     //ne rotira

            if (y > 0)      //ako je broj veci od 0 ide naprijed,inace miruje
                y = 1;      
            else y = 0;

            transform.Rotate(Vector3.forward * x * rotateSpeed);       //rotira oko z osi,u x smjeru brzinom rotateSpeed
            activeAgent.velocity = transform.up * y * speed;        //vektor brzine, za kretanje unaprijed y=1


            if ((Time.time > nextFire) && (z > 0))      //ako je trenutno vrijeme igre vece od nextFire i z>0 pucaj
            {
                nextFire = Time.time + fireRate;    //postavi novi nextFire tako da se na trenutno vrijeme doda minimalni vremenski razmak izmedju metaka
                Instantiate(bullet, transform.position, Quaternion.identity);   //stvori metak s trenutne pozicije
            }


            if (hitU.collider != null)      //ako zraka pogađa asteroid, nacrtaj ju
            {
                //   Debug.Log("U" + hitU.distance);
                line1.enabled = true;
                line1.SetPosition(0, transform.position);
                line1.SetPosition(1, hitU.transform.position);
            }
            else
            {
                line1.enabled = false;
            }
            if (hitL.collider != null)
            {
                //   Debug.Log("L" + hitL.distance);
                line2.enabled = true;
                line2.SetPosition(0, transform.position);
                line2.SetPosition(1, hitL.transform.position);
            }
            else
            {
                line2.enabled = false;
            }
            if (hitR.collider != null)
            {
                //   Debug.Log("R" + hitR.distance);
                line3.enabled = true;
                line3.SetPosition(0, transform.position);
                line3.SetPosition(1, hitR.transform.position);
            }
            else
            {
                line3.enabled = false;
            }
            if (hitD.collider != null)
            {
                //    Debug.Log("D" + hitD.distance);
                line4.enabled = true;
                line4.SetPosition(0, transform.position);
                line4.SetPosition(1, hitD.transform.position);
            }
            else
            {
                line4.enabled = false;
            }
            if (hitUL.collider != null)
            {
                //    Debug.Log("UL" + hitUL.distance);
                line5.enabled = true;
                line5.SetPosition(0, transform.position);
                line5.SetPosition(1, hitUL.transform.position);
            }
            else
            {
                line5.enabled = false;
            }
            if (hitUR.collider != null)
            {
                //   Debug.Log("UR" + hitUR.distance);
                line6.enabled = true;
                line6.SetPosition(0, transform.position);
                line6.SetPosition(1, hitUR.transform.position);
            }
            else
            {
                line6.enabled = false;
            }
            if (hitDL.collider != null)
            {
                //   Debug.Log("DL" + hitDL.distance);
                line7.enabled = true;
                line7.SetPosition(0, transform.position);
                line7.SetPosition(1, hitDL.transform.position);
            }
            else
            {
                line7.enabled = false;
            }
            if (hitDR.collider != null)
            {
                //   Debug.Log("DR" + hitDR.distance);
                line8.enabled = true;
                line8.SetPosition(0, transform.position);
                line8.SetPosition(1, hitDR.transform.position);
            }
            else
            {
                line8.enabled = false;
            }
        }


        if (hasFailed)  //ako je agent poginuo
        {
            if (geneticAlgorithm.GetCurrentGenomeIndex() == (populationSize-1))   //ako je zadnji genom
            {
                EvolveGenomes();  //pozovi funkciju za stvaranje nove populacije
                return;
            }
            NextTestSubject();  //inace idi na sljedeci genom
        }

        activeAgentFitness = MyGUI.score;  //stavi u varijablu broj bodova koji pise na gui-u
        if (activeAgentFitness > generationBestFitness)  //ako je veci od najveceg iz generacije, postavi ga za najveci iz generacije
        {
            generationBestFitness = activeAgentFitness;
        }
        if (activeAgentFitness > bestFitness)  //ako je veci od najveceg, postavi ga za najveci
        {
            bestFitness = activeAgentFitness;
        }
    }



    public void NextTestSubject()   //postavlja fitness trenutnom, dohvaca sljedeci genom i resetira sve vrijednosti 
    {
        if (evolve == 0)   //ako nije evolucija
        {
            geneticAlgorithm.SetGenomeFitness(activeAgentFitness, geneticAlgorithm.GetCurrentGenomeIndex());  //u trenutni genom stavlja fitness koji je imao
        }
        activeAgentFitness = 0.0f;   //resetira fitness
        genome = geneticAlgorithm.GetNextGenome();  //sprema sljedeci genom u genome
        neuralNet.FromGenome(genome, 8,15,3);   //rastavi genom na komponente i postavi te vrijednosti u neurone
        transform.position = defaultP;  //agentu se postavi pocetna pozicija i rotacija
        transform.rotation = defaultR;
        MyGUI.score = 0;
        MyGUI.timer = 0;
        MyGUI.timeHeDied = Time.timeSinceLevelLoad;
        MyGUI.asteroidScore = 0;
        asteroids = GameObject.FindGameObjectsWithTag("Enemy");  //sve objekte koji imaju tag enemy stavi u listu asteroida
        foreach (GameObject asteroid in asteroids)    //unisti svaki asteroid iz liste asteroida
        {
            Destroy(asteroid);
        }
        hasFailed = false;     //postavi hasFailed na false tj. da je agent ziv
    }



    public void EvolveGenomes()   //poziva se izmedu generacija
    {
        geneticAlgorithm.SetGenomeFitness(activeAgentFitness, geneticAlgorithm.GetCurrentGenomeIndex());  //u zadnji genom prije breedanja stavlja fitness koji je imao
        evolve = 1;
        geneticAlgorithm.BreedPopulation();   //glavna funkcija za stvaranje nove populacije
        NextTestSubject();  //funkcija za sljedeci genom
        generationBestFitness = 0;
        evolve = 0;  //vraca vrijednost varijable evolve na 0
    }
}
