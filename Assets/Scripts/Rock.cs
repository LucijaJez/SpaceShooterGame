﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{
    private Rigidbody2D rbRock;
    private float speedRockMin = 2f;
    private float speedRockMax = 5f;
    public float rotationSpeed = 5f;
    public Vector3 vec;


    void Start ()
    {
        rbRock = GetComponent<Rigidbody2D>();
        GameObject player = GameObject.Find("SpaceTriangle");
        rbRock.velocity = (player.transform.position - rbRock.transform.position).normalized * Random.Range(speedRockMin, speedRockMax);
        Physics2D.IgnoreLayerCollision(8, 9);
        vec = new Vector3(0, 0, Random.Range(-rotationSpeed, rotationSpeed));
        float randomSize = Random.Range(0.3f, 0.6f);
        Vector3 size = new Vector3(randomSize, randomSize, 1);
        rbRock.transform.localScale = size;
    }


    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }


    void LateUpdate()
    {
    transform.Rotate(vec);
    }
}
