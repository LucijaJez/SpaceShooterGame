﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flag : MonoBehaviour
{
    private float maxTimeStart = 16;
    private float minTimeStart = 4;
    private float minTime, maxTime;
    private int maxNumOfAsteroids = 4;
    float time;
    int numOfRocks;
    float spawnTime;
    public GameObject rock;
    public bool f1, f2, f3;


    void Start()
    {
        time = 0;
        f1 = f2 = f3 = false;
        minTime = minTimeStart;
        maxTime = maxTimeStart;
        spawnTime = Random.Range(minTime, maxTime);
    }

    public void Reset()
    {
        time = 0;
        f1 = f2 = f3 = false;
        minTime = minTimeStart;
        maxTime = maxTimeStart;
        maxNumOfAsteroids = 4;
        spawnTime = Random.Range(minTime, maxTime);
    }

    void Update()
    {
        if ((int)MyGUI.timer ==0)
            Reset();

        time += Time.deltaTime;
        numOfRocks = GameObject.FindGameObjectsWithTag("Enemy").Length;

        if (f1 == false && MyGUI.minutes == 1)
        {
            maxNumOfAsteroids++;
            maxTime--;
            minTime--;
            f1 = true;
        }

        if (f2 == false && MyGUI.minutes == 2)
        {
            maxNumOfAsteroids++;
            maxTime--;
            minTime--;
            f2 = true;
        }

        if (f3 == false && MyGUI.minutes == 3)
        {
            maxNumOfAsteroids++;
            minTime--;
            maxTime--;
            f3 = true;
        }

        if (time >= spawnTime)
        {
            time = 0;
            if (numOfRocks < maxNumOfAsteroids)
                Instantiate(rock, transform.position, rock.transform.rotation);
            spawnTime = Random.Range(minTime, maxTime);
        }
    }
}
